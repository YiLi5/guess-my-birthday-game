player_name = input("Hi! What is your name?")

guess_number = 1

for guess_number in list(range(1,6)):
    from random import randint
    month_number = randint(1,12)
    from random import randint
    year_number = randint(1924,2004)
    guess_number = str(guess_number)
    month_number = str(month_number)
    year_number = str(year_number)

    print("Guess " + guess_number + " : " + player_name + " were you " + "born in " + month_number + " / " + year_number + " ?")
    response = input("yes or no?")

    if response == "yes":
        print("I knew it!")
        exit()
    elif response == "no" and guess_number == "5":
        print("I have other things to do. Good Bye.")
    else:
        print("Drat! Lemme try again!")
